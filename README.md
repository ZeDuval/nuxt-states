# nuxt-states

[![npm version][npm-version-src]][npm-version-href]
[![npm downloads][npm-downloads-src]][npm-downloads-href]
[![Circle CI][circle-ci-src]][circle-ci-href]
[![Codecov][codecov-src]][codecov-href]
[![Dependencies][david-dm-src]][david-dm-href]
[![Standard JS][standard-js-src]][standard-js-href]

> 

[📖 **Release Notes**](./CHANGELOG.md)

## Features

Leverage [vue-states](https://github.com/sumcumo/vue-states) & [vue-history](https://github.com/sumcumo/vue-history) as module for Nuxt.js.

## Installation

```js
$ npm install nuxt-states
```

## Setup

1. Add the `nuxt-states` dependency with `yarn` or `npm` to your project
2. Add `nuxt-states` to the `modules` section of `nuxt.config.js`
3. Configure it:

```js
{

    modules: [
        // Simple usage
        'nuxt-states',

        // With options
        ['nuxt-states', { /* module options */ }]
    ]
    // With top level options
    states: {
        /* module options */
    }

}
```

## Options

```js
{
    //// vue-history options

    history: {
        // watch for untracked data-changes
        strict: process.env.NODE_ENV === 'development',
        // write a live feed to the console
        feed: typeof window !== 'undefined',
        // filter events before they are written to the history
        filter: null,
        // react to incoming events
        onEvent: null,
    },

    //// vue-states options

    // a models state will be restored
    restoreOnReplace: process.env.NODE_ENV === 'development',
    // equal to Vue mixins, will be applied to every created state model
    mixins: [
        {
            // activate vue-history for all registered state models
            history: true,
        }
    ],
    // registers state models on the vue $root instance
    globalModels: null,
}
```

See [vue-states](https://github.com/sumcumo/vue-states) & [vue-history](https://github.com/sumcumo/vue-history) for all options and features.

---

## Development

1. Clone this repository
2. Install dependencies using `yarn install` or `npm install`
3. Start development server using `npm run dev`

## License

[MIT License](./LICENSE)

Copyright (c) zeduval <sebastian.kempkes@googlemail.com>

<!-- Badges -->
[npm-version-src]: https://img.shields.io/npm/dt/nuxt-states.svg?style=flat-square
[npm-version-href]: https://npmjs.com/package/nuxt-states

[npm-downloads-src]: https://img.shields.io/npm/v/nuxt-states/latest.svg?style=flat-square
[npm-downloads-href]: https://npmjs.com/package/nuxt-states

[circle-ci-src]: https://img.shields.io/circleci/project/github/.svg?style=flat-square
[circle-ci-href]: https://circleci.com/gh/

[codecov-src]: https://img.shields.io/codecov/c/github/.svg?style=flat-square
[codecov-href]: https://codecov.io/gh/

[david-dm-src]: https://david-dm.org//status.svg?style=flat-square
[david-dm-href]: https://david-dm.org/

[standard-js-src]: https://img.shields.io/badge/code_style-standard-brightgreen.svg?style=flat-square
[standard-js-href]: https://standardjs.com
