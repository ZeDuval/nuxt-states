import Vue from 'vue'
import VueStates from '@sum.cumo/vue-states'

<% if (options.history) { %>
import VueHistory from '@sum.cumo/vue-history'
Vue.use(VueHistory, {
    strict: <%= options.history.strict %>,
    feed: <%= options.history.feed %>,
    filter: <% print(options.history.filter || 'null') %>,
    onEvent: <% print(options.history.onEvent || 'null') %>,
})
<% } %>

<% if (options.globalModels) { %>
import globalModels from '<%= options.globalModels %>'
<% }else{ %>
const globalModels = {}
<% } %>

Vue.use(VueStates, {
    restoreOnReplace: <%= options.restoreOnReplace %>,
    mixins: <%= JSON.stringify(options.mixins) %>,
    globalModels,
})