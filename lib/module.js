const { resolve } = require('path')

module.exports = async function (moduleOptions) {

    const mergedOptions = {
        ...this.options['nuxt-states'],
        ...moduleOptions
    }

    const defaultOptions = {
        history: {
            strict: process.env.NODE_ENV === 'development',
            feed: typeof window !== 'undefined',
            filter: null,
            onEvent: null,
        },
        restoreOnReplace: process.env.NODE_ENV === 'development',
        mixins: [
            {
                // abstract: true,
                history: true,
            }
        ],
        globalModels: null,
    }

    const pluginOptions = {
        ...defaultOptions,
        ...mergedOptions
    }

    pluginOptions.mixins = defaultOptions.mixins
    if (mergedOptions.mixins && typeof mergedOptions.mixins === 'object')
        pluginOptions.mixins.push(...mergedOptions.mixins)

    this.addPlugin({
        src: resolve(__dirname, 'plugin.js'),
        fileName: 'nuxt-states.js',
        options: pluginOptions,
    })
}

module.exports.meta = require('../package.json')
